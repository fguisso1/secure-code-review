﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BooksTable.Models;

namespace BooksTable.Database
{
    public static class DbInitializer
    {
        public static async Task Initialize(SchoolContext context)
        {

            await WaitForDb(context);

            context.Database.Migrate();
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Students.Any())
            {
                return;   // DB has been seeded
            }

            var users = new AspNetUser[]
            {
                new AspNetUser(){ Username = "caio", Password = "geolado2021"},
                new AspNetUser(){ Username = "vrech", Password = "gatinhofofo"},
                new AspNetUser(){ Username = "caue", Password = "thebesthack3r"},
                new AspNetUser(){ Username = "matteus", Password = "motherofgod2001"},
                new AspNetUser(){ Username = "raulseixas", Password = "boateazul1987"},
                new AspNetUser(){ Username = "renatao", Password = "123456"},
                new AspNetUser(){ Username = "seujorge", Password = "password"},
                new AspNetUser(){ Username = "alicia", Password = "nooneloving"},
            };


            foreach (var s in users)
            {
                await context.Users.AddAsync(s);
            }
            await context.SaveChangesAsync();

            var students = new Student[]
            {
                new Student { FirstName = "Caio",   LastName = "Leandro",
                    AspNetUserId = users.Single(s => s.Username == "caio").Id,
                     },
                new Student { FirstName = "Matheus", LastName = "Vrechson",
                    AspNetUserId = users.Single(s => s.Username == "vrech").Id,
                    },
                new Student { FirstName = "Caue",   LastName = "Santoro",
                    AspNetUserId = users.Single(s => s.Username == "caue").Id,
                    },
                new Student { FirstName = "Matteus",    LastName = "Salazar",
                    AspNetUserId = users.Single(s => s.Username == "matteus").Id,
                    },
                new Student { FirstName = "Raul",      LastName = "Seixas",
                    AspNetUserId = users.Single(s => s.Username == "raulseixas").Id,
                    },
                new Student { FirstName = "Renato",    LastName = "Ucraniano",
                    AspNetUserId = users.Single(s => s.Username == "renatao").Id,
                    },
                new Student { FirstName = "Jorge",    LastName = "Aragao",
                    AspNetUserId = users.Single(s => s.Username == "seujorge").Id,
                    },
                new Student { FirstName = "Alicia",     LastName = "Chaves",
                    AspNetUserId = users.Single(s => s.Username == "alicia").Id,
                    }
            };

            foreach (Student s in students)
            {
                await context.Students.AddAsync(s);
            }
            await context.SaveChangesAsync();


            var courses = new Course[]
            {
                new Course {Title = "Chemistry",      Credits = 3,
                },
                new Course {Title = "Microeconomics", Credits = 3,
                },
                new Course {Title = "Macroeconomics", Credits = 3,
                },
                new Course {Title = "Calculus",       Credits = 4,
                },
                new Course {Title = "Trigonometry",   Credits = 4,
                },
                new Course {Title = "Composition",    Credits = 3,
                },
                new Course {Title = "Literature",     Credits = 4,
                },
            };

            foreach (Course c in courses)
            {
                await context.Courses.AddAsync(c);
            }
            await context.SaveChangesAsync();

        }


        private static async Task WaitForDb(DbContext context)
        {
            var maxAttemps = 12;
            var delay = 5000;

            var healthChecker = new DbHealthChecker();
            for (int i = 0; i < maxAttemps; i++)
            {
                if (healthChecker.TestConnection(context))
                {
                    return;
                }
                await Task.Delay(delay);
            }

            // after a few attemps we give up
            throw new Exception("Error wating database");

        }
    }
}
