﻿using Microsoft.EntityFrameworkCore;

namespace SqlInjection.Database
{
    public class DbHealthChecker
    {
        public bool TestConnection(DbContext context)
        {

            try
            {
                context.Database.GetPendingMigrations();:w

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
